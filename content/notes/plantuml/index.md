+++
title = "PlantUML"
author = ["Kaushal Modi"]
description = "Collection of PlantUML snippets that I find useful."
date = 2018-04-06
lastmod = 2019-08-02T12:29:51-04:00
tags = ["plantuml"]
draft = false
creator = "Emacs 27.0.50 (Org mode 9.2.3 + ox-hugo)"
[versions]
  plantuml = "1.2018.14"
+++

<div class="ox-hugo-toc toc">
<div></div>

<div class="heading">Table of Contents</div>

- [Class with tree-structure in body](#class-with-tree-structure-in-body)
- [Class Composition](#class-composition)
    - [Reference](#reference)
- [Nested boxes](#nested-boxes)
- [Forcing vertical ordering of rectangles](#forcing-vertical-ordering-of-rectangles)
- [Simple Flowchart](#simple-flowchart)
    - [Activity Diagram](#activity-diagram)
    - [Activity Diagram -- Beta](#activity-diagram-beta)
- [Hyperlinked SVGs](#hyperlinked-svgs)

</div>
<!--endtoc-->



## Class with tree-structure in body {#class-with-tree-structure-in-body}

```plantuml
class Foo {
**Bar(Model)**
|_ prop
|_ **Bom(Model)**
  |_ prop2
  |_ prop3
    |_ prop3.1
|_ prop4
--
}
```

{{< figure src="class-body-tree.png" caption="Figure 1: Class with tree structure in body" >}}

[Source](http://forum.plantuml.net/3448/how-to-display-nested-tree-like-structure-in-a-class-body?show=3455#a3455)


## Class Composition {#class-composition}

Diagram to represent a class containing children classes.

```plantuml
class h-entry << (H,white) >> {
  Post Entry
  ==
  ""<div class="h-entry">..</div>""
}

class p-name << (P,orchid) >> {
  Title of the post
  ==
  ""<h1 class="p-name">..</h1>""
}

class u-url << (U,#7EC0EE) >> {
  Permalink of the post
  ==
  ""<a href=".." class="u-url">..</a>""
}

class u-syndication << (U,#7EC0EE) >> {
  Link where the post is syndicated
  ==
  ""<a href=".." class="u-syndication" rel="syndication">..</a>""
}

class dt-published << (D,green) >> {
  Publish date of the post
  ..
  Go Date/time format: "2006-01-02T15:04:05-0700"
  ==
  ""<time datetime=".." class="dt-published">..</time>""
}

class p-summary << (P,orchid) >> {
  Summary/description of the post
  ==
  ""<div class="p-summary">..</div>""
}

class u-author << (U,#7EC0EE) >> {
  Post author
  ==
  ""<a href="HOMEPAGE" class="u-author">..</a>""
}

class e-content << (E,red) >> {
  Post content
  ==
  ""<div class="e-content">..</div>""
}

class p-category << (P,orchid) >> {
  Tag, category, series name
  ==
  ""<a href="TAGPAGE" class="p-category">TAG</a>""
}

"p-name" -up-* "h-entry"
"u-url" -up-* "h-entry"
"u-syndication" -left-* "h-entry"
"h-entry" *-right- "p-summary"
"h-entry" *-right- "u-author"
"h-entry" *-down- "dt-published"
"h-entry" *-down- "e-content"
"h-entry" *-down- "p-category"
```

{{< figure src="class-composition.png" caption="Figure 2: The `h-entry` object is shown to **contain** all other objects shown in the diagram" >}}


### Reference {#reference}

-   <http://plantuml.com/class-diagram>


## Nested boxes {#nested-boxes}

> I'd like to document the nested HTML tags for my web page, and
> represent them as a diagram with nested blocks.
>
> While searching for an existing representation like that online, I
> came across [this](https://mdn.mozillademos.org/files/14075/bubbling-capturing.png).
>
> Is there a way to draw something like the nested blocks in the center
> of that figure using PlantUML?

```plantuml
rectangle "<html>, <body>, etc." as a  {
  rectangle "<div>..." as b #antiquewhite {
    rectangle "<video>...\n\n\n" as c
  }
}
```

{{< figure src="nested-boxes.png" caption="Figure 3: Depicting nesting of objects in PlantUML" >}}

[Source](http://forum.plantuml.net/7530/how-to-create-a-diagram-with-unconnected-nested-blocks?show=7531#a7531)


## Forcing vertical ordering of rectangles {#forcing-vertical-ordering-of-rectangles}

Trying to nest _rectangles_ within _rectangles_ will give this:

```plantuml
rectangle a {
  rectangle b
  rectangle c
  rectangle d
}
```

{{< figure src="vertically-ordered-rectangles-not-working.svg" caption="Figure 4: Nested rectangles showing up in arbitrary horizontal/vertical arrangement" >}}

To force the vertical order and alignment of _rectangles_, use
**hidden** arrows (`b -[hidden]-> c`).

<div class="note">
  <div></div>

Note that this is a hack, and must not be abused.

</div>

```plantuml
rectangle a {
  rectangle b
  rectangle c
  rectangle d
  b -[hidden]-> c
  c -[hidden]-> d
}
```

{{< figure src="vertically-ordered-rectangles.svg" caption="Figure 5: Forcing the nested rectangles to show up in vertical order using `-[hidden]->` hack" >}}

[Source](http://forum.plantuml.net/7537/specify-nested-rectangles-to-be-created-in-vertical-order?show=7545#a7545)


## Simple Flowchart {#simple-flowchart}


### Activity Diagram {#activity-diagram}

[Activity Diagram](http://plantuml.com/activity-diagram)

```plantuml
(*) -> "First Activity"
-> "Second Activity"
-> "Third Activity"
```

{{< figure src="flowchart-activity-diagram.svg" caption="Figure 6: Simple Flowchart 1" >}}

I'd like to have the above diagram without that "start" dot. But if I
remove it, it generates a _sequence_ diagram:

```plantuml
"First Activity" -> "Second Activity"
-> "Third Activity"
```

{{< figure src="sequence-diagram.svg" caption="Figure 7: Sequence Diagram" >}}


### Activity Diagram -- Beta {#activity-diagram-beta}

[Activity Diagram -- Beta](http://plantuml.com/activity-diagram-beta)

The activity diagram **beta** syntax does almost what I want --- a
horizontal flow chart --- just that it is vertical. The good thing is
that the "start" dot can be omitted using this syntax.

```plantuml
:First Activity;
:Second Activity;
:Third Activity;
```

{{< figure src="flowchart-activity-diagram-beta.svg" caption="Figure 8: Simple Flowchart 2" >}}


## Hyperlinked SVGs {#hyperlinked-svgs}

-   [SVGs with hyperlinks](http://plantuml.com/svg)
-   [Hyperlinks in PlantUML](http://plantuml.com/link)

<!--listend-->

```plantuml
skinparam svgLinkTarget _parent
start
:[[http://plantuml.com PlantUML Homepage]];
stop
```

<svg
xmlns="http://www.w3.org/2000/svg"
xmlns:xlink="http://www.w3.org/1999/xlink"
contentScriptType="application/ecmascript" contentStyleType="text/css"
height="134px" preserveAspectRatio="none"
style="width:170px;height:134px;" version="1.1" viewBox="0 0 170 134"
width="170px" zoomAndPan="magnify"><defs><filter height="300%"
id="fz0jnej1b2o64" width="300%" x="-1" y="-1"><feGaussianBlur
result="blurOut" stdDeviation="2.0"/><feColorMatrix in="blurOut"
result="blurOut2" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
0 0 0 .4 0"/><feOffset dx="4.0" dy="4.0" in="blurOut2"
result="blurOut3"/><feBlend in="SourceGraphic" in2="blurOut3"
mode="normal"/></filter></defs><g><ellipse cx="84.5" cy="20"
fill="#000000" filter="url(#fz0jnej1b2o64)" rx="10" ry="10"
style="stroke: none; stroke-width: 1.0;"/><rect fill="#FEFECE"
filter="url(#fz0jnej1b2o64)" height="33.9688" rx="12.5" ry="12.5"
style="stroke: #A80036; stroke-width: 1.5;" width="149" x="10"
y="50"/><a target="_parent" xlink:actuate="onRequest"
xlink:href="http://plantuml.com" xlink:show="new"
xlink:title="http://plantuml.com" xlink:type="simple"><text
fill="#0000FF" font-family="sans-serif" font-size="12"
lengthAdjust="spacingAndGlyphs" text-decoration="underline"
textLength="129" x="20" y="71.1387">PlantUML Homepage</text><line
style="stroke: #0000FF; stroke-width: 1.0;" x1="20" x2="149"
y1="73.1387" y2="73.1387"/></a><ellipse cx="84.5" cy="113.9688"
fill="none" filter="url(#fz0jnej1b2o64)" rx="10" ry="10"
style="stroke: #000000; stroke-width: 1.0;"/><ellipse cx="85"
cy="114.4688" fill="#000000" filter="url(#fz0jnej1b2o64)" rx="6"
ry="6" style="stroke: none; stroke-width: 1.0;"/><line style="stroke:
#A80036; stroke-width: 1.5;" x1="84.5" x2="84.5" y1="30"
y2="50"/><polygon fill="#A80036"
points="80.5,40,84.5,50,88.5,40,84.5,44" style="stroke: #A80036;
stroke-width: 1.0;"/><line style="stroke: #A80036; stroke-width: 1.5;"
x1="84.5" x2="84.5" y1="83.9688" y2="103.9688"/><polygon
fill="#A80036"
points="80.5,93.9688,84.5,103.9688,88.5,93.9688,84.5,97.9688"
style="stroke: #A80036; stroke-width: 1.0;"/></g></svg>

<div class="figure-caption">
  Figure 9: An inlined SVG with hyperlink
</div>

<div class="note">
  <div></div>

For the hyperlinks in SVGs to work, the SVGs need to be inlined, and
not embedded in the `<img>` HTML tags --- [Reference](https://alligator.io/svg/hyperlinks-svg/).

</div>

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
