+++
title = "PlantUML version"
author = ["Kaushal Modi"]
description = "Finding PlantUML version."
date = 2018-04-03T17:53:00-04:00
tags = ["plantuml", "version"]
draft = false
creator = "Emacs 27.0.50 (Org mode 9.1.9 + ox-hugo)"
+++

```text
java -jar /path/to/plantuml.jar -version
```

That will give an output like:

```text
PlantUML version 1.2018.02 (Fri Mar 09 12:20:44 EST 2018)
(GPL source distribution)
Java Runtime: OpenJDK Runtime Environment
JVM: OpenJDK 64-Bit Server VM
Java Version: 1.7.0_65-mockbuild_2014_07_14_06_19-b00
Operating System: Linux
OS Version: 2.6.32-642.6.2.el6.x86_64
Default Encoding: UTF-8
Language: en
Country: US
Machine: ...
PLANTUML_LIMIT_SIZE: 4096
Processors: 8
Max Memory: 15,011,938,304
Total Memory: 1,011,875,840
Free Memory: 1,001,284,936
Used Memory: 10,590,904
Thread Active Count: 1

The environment variable GRAPHVIZ_DOT has not been set
Dot executable is /usr/bin/dot
Dot version: dot - graphviz version 2.26.0 (20091210.2329)
Installation seems OK. File generation OK
```

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
