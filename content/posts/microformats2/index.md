+++
title = "Microformats2"
author = ["Kaushal Modi"]
series = ["Indiewebifying my Hugo site"]
tags = ["indieweb", "webmention", "microformats2", "h-entry", "h-card", "h-feed", "go-template", "hugo"]
categories = ["web"]
draft = true
creator = "Emacs 27.0.50 (Org mode 9.2.3 + ox-hugo)"
series_weight = 4006
[versions]
  plantuml = "beta 2018/04/09"
+++

<div class="ox-hugo-toc toc">
<div></div>

<div class="heading">Table of Contents</div>

- [Microformats2 prefixes](#microformats2-prefixes)
- [`h-entry`](#h-entry)
    - [`h-entry` ref](#h-entry-ref)
- [`h-card`](#h-card)
    - [`h-card` ref](#h-card-ref)
- [`h-feed`](#h-feed)

</div>
<!--endtoc-->

-   Making your posts be parsed into well-formatted Webmentions when posted as replies to other posts.

-   `h-entry`, `h-card`, `h-feed`
-   Diagram showing the nesting of the h-, p-, u-, e- classes.


## Microformats2 prefixes {#microformats2-prefixes}

`h-*`
:



`p-*`
: plain text

`e-*`
: embedded HTML

`u-*`
:



`dt-*`
:


## `h-entry` {#h-entry}

{{< figure src="h-entry.svg" >}}


### `h-entry` ref {#h-entry-ref}

-   <http://microformats.org/wiki/h-entry>
-   h-entry validation: <https://xray.p3k.io/>
-   <https://indiewebify.me/validate-h-entry/>


## `h-card` {#h-card}


### `h-card` ref {#h-card-ref}

-   authorship testing: <https://sturdy-backbone.glitch.me/>


## `h-feed` {#h-feed}

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
