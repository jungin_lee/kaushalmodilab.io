+++
title = "How to change upper case to lower via search/replace?"
date = "2014-03-14T09:46:58-04:00"
categories = ["emacs", "regex", "replace"]
+++

This posts explain how the emacs in-built `query-replace-regexp`
command can be used to convert upper-cased strings to lower case.

<!--more-->

* Do `M-x query-replace-regexp` or use the default binding `C-M-%` to
  activate the regular expression search/replace.
* Enter the regexp for the strings to be replaced in the *Query regexp:"
  field. For instance, if I want to convert `I_data` and `Q_data` to
  `i_data` and `q_data` respectively, then my search regular expression
  will be `\([IQ]\)_data`.
* It is important to use the **escaped** grouping brackets `\(` and `\)`
  to wrap an expression that you want to upcase or downcase.
* In the next *Query replace:* prompt, the expression will be
  `\,(downcase \1)_data`[^1].

[^1]: Use `\,(upcase \REGEXGROUPNUMBER)` to convert to upper case instead.
