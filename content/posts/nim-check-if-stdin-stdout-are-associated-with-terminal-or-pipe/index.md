+++
title = "Nim: Check if stdin/stdout are associated with terminal or pipe"
author = ["Kaushal Modi"]
description = """
  When writing bash scripts, I often need to know if the script is
  receiving input from the terminal, or some piped process. I would also
  need to know if the script is sending output to the terminal, or
  to another piped process.

  As I am learning Nim and trying to write new scripts using that, I
  need to know how to do the same in Nim.
  """
date = 2018-05-15T13:12:00-04:00
tags = ["nim", "stdin", "stdout", "tty", "terminal", "pipe", "bash"]
categories = ["programming"]
draft = false
creator = "Emacs 27.0.50 (Org mode 9.1.14 + ox-hugo)"
[versions]
  nim = "0.18.1"
[syndication]
  twitter = 996439608903831553
+++

<div class="ox-hugo-toc toc">
<div></div>

<div class="heading">Table of Contents</div>

- [Bash](#bash)
- [Nim](#nim)
    - [Using `os.getFileInfo`](#using-os-dot-getfileinfo)
    - [Using `terminal.isatty`](#using-terminal-dot-isatty)
- [Result](#result)

</div>
<!--endtoc-->

Today, I came across this **r/nim** [thread](https://www.reddit.com/r/nim/comments/8jki3k/checking%5Fstdin%5Ffor%5Fcontent/) where a user needed to know
if the Nim-compiled binary was receiving input from _stdin_.

That reminded me of [this technique](https://github.com/kaushalmodi/eless/blob/0065c3b9629bcc74b5d1f818336d9dcf1ccd2b05/eless#L166-L196) that I had used in my bash project
[**eless**](https://eless.scripter.co/)..  (by checking `[[ -t 0 ]]` and `[[ -t 1 ]]`).


## Bash {#bash}

Here's a similar bash snippet if you want to try it out:

<a id="code-snippet--stdin-stdout-bash"></a>
```bash
#!/usr/bin/env bash

# How to detect whether input is from keyboard, a file, or another process.
# Useful for writing a script that can read from standard input, or prompt the
# user for input if there is none.

# https://gist.github.com/davejamesmiller/1966557
if [[ -t 0 ]] # Script is called normally - Terminal input (keyboard) - interactive
then
    # eless foo
    # eless foo | cat -
    echo "--> Input from terminal"
else # Script is getting input from pipe or file - non-interactive
    # echo bar | eless foo
    # echo bar | eless foo | cat -
    echo "--> Input from PIPE/FILE"
fi

# https://stackoverflow.com/a/911213/1219634
if [[ -t 1 ]] # Output is going to the terminal
then
    # eless foo
    # echo bar | eless foo
    echo "    Output to terminal -->"
else # Output is going to a pipe, file?
    # eless foo | cat -
    # echo bar | eless foo | cat -
    echo "    Output to a PIPE -->"
fi
```

<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--stdin-stdout-bash">Code Snippet 1</a></span>:
  Checking <b>stdin</b> and <b>stdout</b> in bash
</div>

So using that as the basis, I went down the path of figuring out how
to do the same in Nim.


## Nim {#nim}

I remembered seeing the variables [`stdin`](https://nim-lang.org/docs/system.html#stdin) and [`stdout`](https://nim-lang.org/docs/system.html#stdout) defined in the
implicitly[^fn:1] imported `system` module. So I started with trying
to get the "value" of those variables and see if they help me figure
out if they are associated with the terminal or not.


### Using `os.getFileInfo` {#using-os-dot-getfileinfo}

From the docs of `stdin` and `stdout`, I learned that the **type** of
both of those is `File`. So while searching for "File", I ended up on
[`os.FileInfo`](https://devdocs.io/nim/os#FileInfo), whose value I can get using [`os.getFileInfo`](https://devdocs.io/nim/os#getFileInfo).

Based on the structure of that `FileInfo` object:

{{< highlight nim "hl_lines=2" >}}
FileInfo = object
  id*: tuple[device: DeviceId, file: FileId]
  kind*: PathComponent
  size*: BiggestInt
  permissions*: set[FilePermission]
  linkCount*: BiggestInt
  lastAccessTime*: times.Time
  lastWriteTime*: times.Time
  creationTime*: times.Time
{{< /highlight >}}

I guessed that the `id` field in there might hold the information I
needed.. and indeed it did!

By printing out the value of `getFileInfo(stdin).id.file`, and running
the Nim-compiled binary by itself (`./binary`) _vs_ providing it the
output of another process (`echo foo | ./binary`), I learned that its
value is always `37` if the binary is not receiving input from another
process/pipe.

_Similarly the value of `getFileInfo(stdout).id.file` is always `37`
if the binary is not sending the output to another process/pipe._

Based on that deduction, this worked!

<a id="code-snippet--stdin-stdout-getFileInfo"></a>
```nim
# Figuring out if input is coming from a pipe and if output is going to a pipe.

import os

# https://nim-lang.org/docs/os.html#FileInfo
if getFileInfo(stdin).id.file==37:
  # ./stdin_stdout foo
  # ./stdin_stdout foo | cat
  # echo "--> Input from terminal: ", readAll(stdin) # Gets stuck till user types Ctrl+d (EOF)
  echo "--> Input from terminal"
else:
  # echo bar | ./stdin_stdout
  # echo bar | ./stdin_stdout | cat
  echo "--> Input from a PIPE/FILE"

if getFileInfo(stdout).id.file==37:
  # ./stdin_stdout foo
  # echo bar | ./stdin_stdout foo
  echo "    Output to terminal -->"
else:
  # ./stdin_stdout | cat
  # echo bar | ./stdin_stdout | cat
  echo "    Output to a PIPE -->"
```

<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--stdin-stdout-getFileInfo">Code Snippet 2</a></span>:
  Using <code>os.getFileInfo</code> to check <b>stdin</b> and <b>stdout</b>
</div>


### Using `terminal.isatty` {#using-terminal-dot-isatty}

In the same reddit thread, [**/u/bpbio**](https://www.reddit.com/user/bpbio) from Reddit provides a better,
concise [answer](https://www.reddit.com/r/nim/comments/8jki3k/checking%5Fstdin%5Ffor%5Fcontent/dz0pmjd/)---using the `isatty` proc from the `terminal` module:

> ```nim
> proc isatty(f: File): bool {.raises: [], tags: [].}
> ```
>
> Returns true if f is associated with a terminal device.

From my brief testing, I saw that `isatty(stdin)` is equivalent to
`getFileInfo(stdin).id.file==37`, and the same for `stdout` too.

So my above snippet can be rewritten as:

<a id="code-snippet--stdin-stdout-isatty"></a>
{{< highlight nim "hl_lines=3 5 14" >}}
# Figuring out if input is coming from a pipe and if output is going to a pipe.

import terminal

if isatty(stdin):
  # ./stdin_stdout foo
  # ./stdin_stdout foo | cat
  echo "--> Input from terminal"
else:
  # echo bar | ./stdin_stdout
  # echo bar | ./stdin_stdout | cat
  echo "--> Input from a PIPE/FILE"

if isatty(stdout):
  # ./stdin_stdout foo
  # echo bar | ./stdin_stdout foo
  echo "    Output to terminal -->"
else:
  # ./stdin_stdout | cat
  # echo bar | ./stdin_stdout | cat
  echo "    Output to a PIPE -->"
{{< /highlight >}}


<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--stdin-stdout-isatty">Code Snippet 3</a></span>:
  Using <code>terminal.isatty</code> to check <b>stdin</b> and <b>stdout</b>
</div>


## Result {#result}

Assuming that the Nim-compiled binary of the above code[^fn:2] is
`some_exe`, you get this output:

````text
> ./some_exe
--> Input from terminal
    Output to terminal -->
> ./some_exe | cat
--> Input from terminal
    Output to a PIPE -->
> echo foo | ./some_exe
--> Input from a PIPE/FILE
    Output to terminal -->
> echo foo | ./some_exe | cat
--> Input from a PIPE/FILE
    Output to a PIPE -->
````

[^fn:1]: Nim has the concept of implictly and explicitly imported modules. You do not need to manually import the former using the `import` keyword, while you need to do that for the latter.
[^fn:2]: It doesn't matter if you use [Code Snippet 2](#code-snippet--stdin-stdout-getFileInfo) or [Code Snippet 3](#code-snippet--stdin-stdout-isatty).

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
