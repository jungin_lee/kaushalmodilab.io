#+title: awk
#+date: 2018-05-02

#+setupfile: ../../scripter-setupfile.org

#+hugo_base_dir: ../../../
#+hugo_section: notes
#+hugo_bundle: awk
#+export_file_name: index

#+hugo_categories: unix
#+hugo_tags: awk

#+hugo_auto_set_lastmod: t

#+property: header-args:awk :exports both :results verbatim :in-file emp.data

#+options: toc:3

#+begin_description
Collection of ~awk~ examples.
#+end_description

* Simple example
#+begin_src awk :exports both :results verbatim
BEGIN { print 42 }
#+end_src

#+results:
: 42

* The AWK Programming Language
This section contains awk examples and my notes from the /The AWK
Programming Language/ book by Alfred V. Aho, Brian W. Kernighan and
Peter J. Weinberger.
** An AWK Tutorial
#+caption: Example Input used for examples
#+name: code__emp_data
#+begin_src text :tangle emp.data :eval never
Beth    4.00   0
Dan     3.75   0
Kathy   4.00   10
Mark    5.00   20
Mary    5.50   22
Susie   4.25   18
#+end_src
# Tangle the above block to the emp.data file by running
# org-babel-tangle (C-c C-v t).
*** COMMENT Alternative way to provide input to awk
#+caption: Example employee data
#+name: tab__emp_data
| Beth  | 4.00 |  0 |
| Dan   | 3.75 |  0 |
| Kathy | 4.00 | 10 |
| Mark  | 5.00 | 20 |
| Mary  | 5.50 | 22 |
| Susie | 4.25 | 18 |
# #+begin_src awk :stdin tab__emp_data :exports both :results verbatim
# $3 > 0 { print $1, $2 * $3 }
# #+end_src
*** Getting Started
#+caption: Print total salary only for the employees who have worked for non-zero hours
#+name: code__1_1__1
#+begin_src awk
$3 > 0 { print $1, $2 * $3 }
#+end_src

#+results: code__1_1__1
: Kathy 40
: Mark 100
: Mary 121
: Susie 76.5
The =,= between =$1= and =$2= renders as a space in the output by default. That can be changed.
#+caption: Print employees who did not work
#+name: code__1_1__2
#+begin_src awk
$3 == 0 { print $1 }
#+end_src

#+results: code__1_1__2
: Beth
: Dan
**** The Structure of an AWK Program
Each awk program in this chapter is a sequence of one or more pattern-action statements:
#+begin_example
pattern { action }
pattern { action }
...
#+end_example
Either the /pattern/ or the /action/ (but not both) in a
pattern-action statement may be omitted. If a pattern has no action as
in [[code__1_1__3]], then each line that the pattern matches is printed.
#+caption: Pattern without action
#+name: code__1_1__3
#+begin_src awk
$3 == 0
#+end_src

#+results: code__1_1__3
: Beth    4.00   0
: Dan     3.75   0
And if there is an action with no pattern, then the action is performed for every input line.
#+caption: Action without pattern
#+name: code__1_1__4
#+begin_src awk
{ print $1 }
#+end_src

#+results: code__1_1__4
: Beth
: Dan
: Kathy
: Mark
: Mary
: Susie

*** Simple Output
**** Printing Every Line
#+caption: Printing every line -- 1
#+name: code__1_2__1
#+begin_src awk
{ print }
#+end_src

#+results: code__1_2__1
: Beth    4.00   0
: Dan     3.75   0
: Kathy   4.00   10
: Mark    5.00   20
: Mary    5.50   22
: Susie   4.25   18

#+caption: Printing every line -- 2
#+name: code__1_2__2
#+begin_src awk
{ print $0 }
#+end_src

#+results: code__1_2__2
: Beth    4.00   0
: Dan     3.75   0
: Kathy   4.00   10
: Mark    5.00   20
: Mary    5.50   22
: Susie   4.25   18
**** Printing Certain Fields
#+caption: Printing certain fields
#+name: code__1_2__3
#+begin_src awk
{ print $1, $3 }
#+end_src

#+results: code__1_2__3
: Beth 0
: Dan 0
: Kathy 10
: Mark 20
: Mary 22
: Susie 18
**** =NF=, the Number of Fields
#+caption: Print number of fields, first field and /last field/ for each input line
#+name: code__1_2__4
#+begin_src awk
{ print NF, $1, $NF }
#+end_src

#+results: code__1_2__4
: 3 Beth 0
: 3 Dan 0
: 3 Kathy 10
: 3 Mark 20
: 3 Mary 22
: 3 Susie 18
**** Computing and Printing
#+caption: Do computations on field values
#+name: code__1_2__5
#+begin_src awk
{ print $1, $2 * $3 }
#+end_src

#+results: code__1_2__5
: Beth 0
: Dan 0
: Kathy 40
: Mark 100
: Mary 121
: Susie 76.5
**** Printing Line Numbers
#+caption: =NR=, Number of lines Read
#+name: code__1_2__6
#+begin_src awk
{ print NR, $0 }
#+end_src

#+results: code__1_2__6
: 1 Beth    4.00   0
: 2 Dan     3.75   0
: 3 Kathy   4.00   10
: 4 Mark    5.00   20
: 5 Mary    5.50   22
: 6 Susie   4.25   18
**** Putting Text in the Output
#+caption: Concatenating text in the output
#+name: code__1_2__7
#+begin_src awk
{ print "total pay for", $1, "is", $2 * $3 }
#+end_src

#+results: code__1_2__7
: total pay for Beth is 0
: total pay for Dan is 0
: total pay for Kathy is 40
: total pay for Mark is 100
: total pay for Mary is 121
: total pay for Susie is 76.5

*** Fancier Output
- The =print= statement is for quick and easy output.
- The =printf= statement is used if you need to format the output exactly the way you want.
**** Lining Up Fields
With =printf=, no blanks or newlines are produced automatically; you
need to create them yourself. /Note the =\n= in the =printf= statement
in [[code__1_3__1]]./

#+caption: =printf= example
#+name: code__1_3__1
#+begin_src awk
{ printf("total pay for %s is $%.2f\n", $1, $2 * $3) }
#+end_src

#+results: code__1_3__1
: total pay for Beth is $0.00
: total pay for Dan is $0.00
: total pay for Kathy is $40.00
: total pay for Mark is $100.00
: total pay for Mary is $121.00
: total pay for Susie is $76.50

#+caption: Justification using =printf=
#+name: code__1_3__2
#+begin_src awk
{ printf("%-8s $%6.2f\n", $1, $2 * $3) }
#+end_src

#+results: code__1_3__2
: Beth     $  0.00
: Dan      $  0.00
: Kathy    $ 40.00
: Mark     $100.00
: Mary     $121.00
: Susie    $ 76.50

*** Selection
**** Selection by Comparison
#+caption: awk program with just a comparison pattern
#+name: code__1_4__1
#+begin_src awk
$2 >= 5
#+end_src

#+results: code__1_4__1
: Mark    5.00   20
: Mary    5.50   22
**** Selection by Computation
#+caption: Print details only for employees making more than $50
#+name: code__1_4__2
#+begin_src awk
$2 * $3 > 50 { printf("$%.2f for %s\n", $2 * $3, $1) }
#+end_src

#+results: code__1_4__2
: $100.00 for Mark
: $121.00 for Mary
: $76.50 for Susie
**** Selection by Text Content
#+caption: Literal string match
#+name: code__1_4__3
#+begin_src awk
$1 == "Susie"
#+end_src

#+results: code__1_4__3
: Susie   4.25   18

#+caption: Regular expression match -- 1
#+name: code__1_4__3__2
#+begin_src awk
/y/
#+end_src

#+results: code__1_4__3__2
: Kathy   4.00   10
: Mary    5.50   22

#+caption: Regex OR expression
#+name: code__regex_or
#+begin_src awk
/Mary|Beth/
#+end_src

#+RESULTS: code__regex_or
: Beth    4.00   0
: Mary    5.50   22

It looks like regular expressions cannot be specified in relation to
field variables like =$1=, =$2=, etc. But I am most likely wrong. For
instance, the =/y.*4/= in [[code__1_4__3__3]] does a match across fields.
#+caption: Regular expression match -- 2
#+name: code__1_4__3__3
#+begin_src awk
/y.*4/
#+end_src

#+results: code__1_4__3__3
: Kathy   4.00   10

#+caption: Regular expression *not* matching
#+name: code__regex_not_matching
#+begin_src awk
!/y/
#+end_src

#+RESULTS: code__regex_not_matching
: Beth    4.00   0
: Dan     3.75   0
: Mark    5.00   20
: Susie   4.25   18

**** Combinations of Patterns
Patterns can be combined with parentheses and the logic operators =&&= (/and/), =||= (/or/) and =!= (/not/).
#+caption: Logical operators in patterns
#+name: code__1_4__4
#+begin_src awk
$2 >= 4 || $3 >= 20
#+end_src

#+results: code__1_4__4
: Beth    4.00   0
: Kathy   4.00   10
: Mark    5.00   20
: Mary    5.50   22
: Susie   4.25   18
Above, the lines that match both =$2>=4= and =$3>=20= conditions are
printed just once. But in the case of [[code__1_4__5]], where /multiple/
patterns are specified, the program prints a line twice if that line
matches both the conditions.
#+caption: Multiple patterns
#+name: code__1_4__5
#+begin_src awk
$2 >= 4
$3 >= 20
#+end_src

#+results: code__1_4__5
: Beth    4.00   0
: Kathy   4.00   10
: Mark    5.00   20
: Mark    5.00   20
: Mary    5.50   22
: Mary    5.50   22
: Susie   4.25   18
[[code__1_4__6]] is a De Morgan's law variant of [[code__1_4__4]]. Note that
the results are the exact same.
#+caption: Logical operators in patterns
#+name: code__1_4__6
#+begin_src awk
!($2 < 4 && $3 < 20)
#+end_src

#+results: code__1_4__6
: Beth    4.00   0
: Kathy   4.00   10
: Mark    5.00   20
: Mary    5.50   22
: Susie   4.25   18
**** Data Validation
When doing data validation, the lines are printed only when they do not match the desirable properties. Think of this use as that of assertions in SystemVerilog. Below, as any of the lines in the example input do not match the failure conditions, there is no output.
#+caption: Data validation or Assertions
#+name: code__1_4__7
#+begin_src awk
NF != 3   { print $0, "number of fields is not equal to 3" }
$2 < 3.35 { print $0, "rate is below minimum wage" }
$2 > 10   { print $0, "rate exceeds $10 per hour" }
$3 < 0    { print $0, "negative hours worked" }
$3 > 60   { print $0, "too many hours worked" }
#+end_src

#+results: code__1_4__7
**** BEGIN and END
- The special pattern =BEGIN= matches *before the first line* of the first input file is read.
- =END= matches *after the last line* of the last file has been processed.
#+caption: Using =BEGIN= to print heading
#+name: code__1_4__8
#+begin_src awk
BEGIN { print "NAME    RATE   HOURS"; print "" }
      { print }
#+end_src

#+results: code__1_4__8
: NAME    RATE   HOURS
:
: Beth    4.00   0
: Dan     3.75   0
: Kathy   4.00   10
: Mark    5.00   20
: Mary    5.50   22
: Susie   4.25   18
As noted from [[code__1_4__8]],
- You can put several statements on a single line if you separate them by semi-colons.
- The =print ""= prints a blank line.
- Plain =print= prints the *whole* line.
*** Computing with AWK
**** Counting
- The user-created variables are not declared; you just use them.
- The default initial value of variables used as numbers (=awk= auto-detects that) is 0.
#+caption: User-created variables
#+name: code__1_5__1
#+begin_src awk
$3 > 15 { emp = emp + 1 }
END     { print emp, "employees worked more than 15 hours" }
#+end_src

#+results: code__1_5__1
: 3 employees worked more than 15 hours
**** Computing Sums and Averages
#+caption: Print the number of lines
#+name: code__1_5__2
#+begin_src awk
END { print NR, "employees" }
#+end_src

#+results: code__1_5__2
: 6 employees

#+caption: Using =NR= to compute the average pay
#+name: code__1_5__3
#+begin_src awk
    { pay = pay + $2 * $3 } # Nothing is printed by this line; only calculation happens
END { print NR, "employees"
      print "total pay is", pay
      print "average pay is", pay/NR
    }
#+end_src

#+results: code__1_5__3
: 6 employees
: total pay is 337.5
: average pay is 56.25
**** Handling Text
#+caption: Find the employee who is paid the most per hour
#+name: code__1_5__4
#+begin_src awk
$2 > maxrate { maxrate = $2; maxemp = $1 } # Here maxrate and maxemp variables are updated conditionally; nothing is printed
END          { print "highest hourly rate:", maxrate, "for", maxemp }
#+end_src

#+results: code__1_5__4
: highest hourly rate: 5.50 for Mary
**** String Concatenation
#+caption: Concatenate strings with spaces in-between
#+name: code__1_5__5
#+begin_src awk
    { names = names $1 " " }
END { print names }
#+end_src

#+results: code__1_5__5
: Beth Dan Kathy Mark Mary Susie
Awk automagically figures out that here the =names= variable is used to hold string and sets its initial value to a /null/ or empty string.
**** Printing the Last Input Line
- Although =NR= retains its values in an =END= action, =$0= *does not*.

So in the below code snippet, we use a user-defined variable =last= to store the =$0= value of the last line read.
#+caption: Print the last line
#+name: code__1_5__6
#+begin_src awk
    { last = $0 }
END { print last }
#+end_src

#+results: code__1_5__6
: Susie   4.25   18
**** Built-in Functions
#+caption: In-built function =length=
#+name: code__1_5__7
#+begin_src awk
{ print $1, length($1) }
#+end_src

#+results: code__1_5__7
: Beth 4
: Dan 3
: Kathy 5
: Mark 4
: Mary 4
: Susie 5
**** Counting Lines, Words and Characters
#+caption: Count lines, words, chars
#+name: code__1_5__8
#+begin_src awk
    { nc = nc + length($0) + 1 # the trailing "+ 1" is to count the newline character for each line
                               # $0 does not include the newline
      nw = nw + NF
    }
END { print NR, "lines", nw, "words", nc, "characters" }
#+end_src

#+results: code__1_5__8
: 6 lines 18 words 106 characters
*** Control-Flow Statements
*The control flow statements can be used only in actions.*
**** If-Else Statement
[[code__1_6__1]] is similar to [[code__1_5__3]], but with an =if= to protect
against division by zero when computing average.
#+caption: Sum and average pay of employees making more than $6/hr
#+name: code__1_6__1
#+begin_src awk
$2 > 6 { n = n + 1; pay = pay + $2 * $3 }
END    { if (n > 0)
           printf("%d employees, total pay is %.2f, average pay is %.2f",
                  n, pay, pay/n) # Note that we can continue a long statement over several lines
                                 # by breaking it after a comma.
         else
           print "no employees are paid more than $6/hour"
       }
#+end_src

#+results: code__1_6__1
: no employees are paid more than $6/hour
**** While Statement
#+caption: Input for interest1 program
#+name: tab__interest1_inp
| 1000 | .06 | 5 |
| 1000 | .12 | 7 |
#+caption: Calculate compound interest
#+name: code__1_6__2
#+begin_src awk :stdin tab__interest1_inp :in-file
# interest1 - compute compound interest
#  input: amount rate years
#  output: compounded value at the end of each year
{ i = 1
  printf("Amount = %.2f, Rate = %.2f, Years = %.2f\n", $1, $2, $3)
  while (i <= $3) {
    printf("\tYear %d: %.2f\n", i, $1 * (1 + $2) ^ i)
    i = i + 1
  }
  print ""
}
#+end_src

#+results: code__1_6__2
#+begin_example
Amount = 1000.00, Rate = 0.06, Years = 5.00
	Year 1: 1060.00
	Year 2: 1123.60
	Year 3: 1191.02
	Year 4: 1262.48
	Year 5: 1338.23

Amount = 1000.00, Rate = 0.12, Years = 7.00
	Year 1: 1120.00
	Year 2: 1254.40
	Year 3: 1404.93
	Year 4: 1573.52
	Year 5: 1762.34
	Year 6: 1973.82
	Year 7: 2210.68

#+end_example
**** COMMENT For Statement
*** COMMENT Arrays
*** COMMENT A Handful of Useful "One-liners"
* COMMENT Snippets
#+caption:
#+name: code__
#+begin_src awk

#+end_src

#+results: code__
