# Rules for naming keys (variables) and table names in this config

1. The base-level keys (**not** table names) should be kept in letter case used
   to define them internally in hugo.

    Here is the [list of all *template variables*][1].

    The reason is that while you can have base-level keys (**not** table names)
   in **any** letter case in `config.toml`, when referencing them in the
   templates, they **have** to have the exact case used internally.

        baseurl = "http://example.org' # BAD  -- cannot be accessed as  --> .Site.baseurl
        baseurl = "http://example.org' # OK   -- accessed only as       --> .Site.BaseURL
        BaseURL = "http://example.org' # GOOD -- accessed only as       --> .Site.BaseURL

2. The base-level table names should also be kept in the letter case used to
   define them internally in hugo, for the same reason above.

        [params] # OK
          VAR                      -- cannot be accessed as  --> .Site.params.VAR
                                   -- accessed only as       --> .Site.Params.VAR
        [Params] # GOOD
          VAR                      -- accessed only as       --> .Site.Params.VAR

3. In summary, for these base-level keys and table names, they have to be
   accessed in the **exact case used internally in hugo** with a `.Site` prefix.

    Below example shows the recommended way of using them in `config.toml`.

        Title = "awesome"                -- accessed only as --> .Site.Title
        BaseURL = "http://example.org'   -- accessed only as --> .Site.BaseURL
        [Params]
          VAR                            -- accessed only as --> .Site.Params.VAR

4. You cannot have custom base-level keys or tables.

    If you try to do that, you will get an error like below:

    > .. executing "after_main" at <.Site.Foo>: can't evaluate field Foo in type **hugolib.SiteInfo

    So put all your custom variables inside the `[Params]` table!

5. You can have the custom variables with any name and in any letter case inside
   `[Params]`, but ensure that you use the **exact same case** when accessing them
   in templates.

        [Params]
          SomeVar = "foo"      -- accessed only as --> .Site.Params.SomeVar
          somevar = "bar"      -- accessed only as --> .Site.Params.somevar

6. Now.. not all inbuilt variables set in `config.toml` are accessible in templates.

    For some reason, even though `theme` is a valid `config.toml` variable,
   you cannot access it using `.Site.Theme`. So to check if you can access
   a variable using the `.Site` prefix, refer to [the list of *template
   variables*][1].

[1]: https://gohugo.io/templates/variables/
